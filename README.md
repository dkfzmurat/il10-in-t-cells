Software for the downstream analysis of ATAC-seq and microarray dataset from the study of
'Loss of IL-10/STAT3 signaling aggravates CD8+ T-cell exhaustion and impedes control of chronic lymphocytic leukemia'

The analysis of microarray and ATAC-seq dataset for the study 'Loss of IL-10/STAT3 signaling aggravates CD8+ T-cell exhaustion and impedes control of chronic lymphocytic leukemia' was developed by Murat Iskar and Marc Zapatka (Lichter lab, DKFZ). The source code used in the analysis of microarray and ATAC-seq dataset is released under the GNU General Public License v3.0. The analysis of 'IL10-signaling-in-T-cell-exhaustion' is Copyright (C) 2018 Murat Iskar, Marc Zapatka and DKFZ. 
