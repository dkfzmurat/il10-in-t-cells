##  This source code file is part of the analysis of microarray and ATAC-seq dataset for the study 'Loss of IL-10/STAT3 signaling aggravates CD8+ T-cell exhaustion and impedes control of chronic lymphocytic leukemia'.
##  Copyright (C) 2018  Murat Iskar, Marc Zapatka
## 
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
## 
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
## 
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.
##
##  Please check the publication "Loss of IL-10/STAT3 signaling aggravates CD8+ T-cell exhaustion and impedes control of chronic lymphocytic leukemia",
##  and the supplementary website: update***https://gitlab.com/dkfzmurat/il10-in-t-cells***
##
## microarray-differential-analysis.R

options(max.print = 200)
options(stringsAsFactors = FALSE)

library(limma)

setwd("~/Projects/CLL_exhaustion/scripts")

expmat <- read.table("../results/microarray/data_XXX/rma.summary.txt",header=TRUE,skip="#",sep="\t")
rownames(expmat)=expmat[,1]
expmat=expmat[,-1]

det<-read.table("../results/microarray/data_XXX/dabg.summary.txt",header=TRUE,sep="\t")
rownames(det)=det[,1]
det=det[,-1]
gr1<-det[,grep("memory",colnames(det))]
gr2<-det[,grep("naive",colnames(det))]
gr3<-det[,grep(".hi.",colnames(det))]
gr4<-det[,grep(".int.",colnames(det))]
pre1<-apply(gr1<0.01,1,sum)
pre2<-apply(gr2<0.01,1,sum)
pre3<-apply(gr3<0.01,1,sum)
pre4<-apply(gr4<0.01,1,sum)
tot<-((pre1>1)|(pre2>1)|(pre3>1)|(pre4>1))

#Filtering:
expmat <- expmat[tot,]

#Limma analysis:
# probesets should be filtered whether they are main or not:
#  n=18 control->affx n=18 control->affx->bac_spike n=39 control->affx->polya_spike  n=23 control->bgp->antigenomic  n=44594 main
# n=1758 normgene->exon n=13972 normgene->intron n= 82 reporter  n=14690 rescue  n=2 rrna
#in total around ~75000
#main: probesets which are a part of the main design for which the array was designed
#normgene->exon: probe sets against exon regions of a set of housekeeping genes
#normgene->intron: probe sets against intron regions of a set of housekeeping genes
#control->affx: standard Affymetrix spike control probeset (ie bacterial and polyA spikes)
#control->bgp->antigenomic: antigenomic background probes
#control->bgp->genomic: genomic background probes
#rescue->FLmRNA->unmapped: probesets against mRNA sequences which did not align to the genome
geneannot <-read.table("../results/downstream-analysis/gene-annotation/MoGene-2_0-st-v1.na35.mm10.transcript.csv/MoGene-2_0-st-v1.na35.mm10.transcript.csv",sep=",",header=TRUE)
#table(geneannot$category)
maingenes=geneannot$transcript_cluster_id[geneannot$category=="main"]
expmat = expmat[rownames(expmat)%in%maingenes,]

save(expmat,file=paste0("../results/downstream-analysis/differential-analysis/",Sys.Date(),"filtered-rma-matrix-for-limma.RData"))
#significant cases of differentiallly regulated genes
treatment=rep(0,16)
treatment[grep("memory",colnames(det))]=1
treatment[grep("naive",colnames(det))]=2
treatment[grep(".int.",colnames(det))]=3
treatment[grep(".hi.",colnames(det))]=4
treatment<-factor(c(treatment))
design <- model.matrix(~0+treatment)
#experiment labels:
cbind(colnames(expmat),treatment)

# treatment
# [1,] "X1J2.AT1.memory.CD8_.MoGene.2_0.st..CEL"   "1"      
# [2,] "X1J2.AT1.naive.CD8_.MoGene.2_0.st..CEL"    "2"      
# [3,] "X1J2.AT1.PD.1.hi.CD8_.MoGene.2_0.st..CEL"  "4"      
# [4,] "X1J2.AT1.PD.1.int.CD8_.MoGene.2_0.st..CEL" "3"      
# [5,] "X1J2.AT2.memory.CD8_.MoGene.2_0.st..CEL"   "1"      
# [6,] "X1J2.AT2.naive.CD8_.MoGene.2_0.st..CEL"    "2"      
# [7,] "X1J2.AT2.PD.1.hi.CD8_.MoGene.2_0.st..CEL"  "4"      
# [8,] "X1J2.AT2.PD.1.int.CD8_.MoGene.2_0.st..CEL" "3"      
# [9,] "X1J2.AT3.memory.CD8_.MoGene.2_0.st..CEL"   "1"      
# [10,] "X1J2.AT3.naive.CD8_.MoGene.2_0.st..CEL"    "2"      
# [11,] "X1J2.AT3.PD.1.hi.CD8_.MoGene.2_0.st..CEL"  "4"      
# [12,] "X1J2.AT3.PD.1.int.CD8_.MoGene.2_0.st..CEL" "3"      
# [13,] "X1J2.AT5.memory.CD8_.MoGene.2_0.st..CEL"   "1"      
# [14,] "X1J2.AT5.naive.CD8_.MoGene.2_0.st..CEL"    "2"      
# [15,] "X1J2.AT5.PD.1.hi.CD8_.MoGene.2_0.st..CEL"  "4"      
# [16,] "X1J2.AT5.PD.1.int.CD8_.MoGene.2_0.st..CEL" "3"

fit <- lmFit(expmat, design)
contrast.matrix <- makeContrasts(treatment4-treatment3, treatment4-treatment1, treatment3-treatment1, levels=design)
fit2 <- contrasts.fit(fit, contrast.matrix)
fit2 <- eBayes(fit2)

result1<-topTable(fit2,number=length(expmat[,1]), coef=1, adjust="fdr")
result1<-result1[order(rownames(result1)),]

result2<-topTable(fit2,number=length(expmat[,1]), coef=2, adjust="fdr")
result2<-result2[order(rownames(result2)),]

result3<-topTable(fit2,number=length(expmat[,1]), coef=3, adjust="fdr")
result3<-result3[order(rownames(result3)),]

allres<-cbind(result1,result2,result3)

save(result1,result2,result3,file=paste0("../results/downstream-analysis/differential-analysis/",Sys.Date(),"differential-analysis-results.RData"))
